// Soal No. 1 Membuat Fungsi Teriak
// Tulislah sebuah function dengan nama teriak() yang mengembalikan nilai “Halo Sanbers!” yang kemudian dapat ditampilkan di console.
console.log("===============================================");
console.log("Soal No. 1 Membuat Fungsi Teriak");
console.log("===============================================");

const teriak = () => "Halo Sanbers!";
console.log(teriak()) // "Halo Sanbers!" 



// Soal No.2 Membuat Fungsi Kalikan
// Tulislah sebuah function dengan nama kalikan() yang mengembalikan hasil perkalian dua parameter yang di kirim.
console.log("\n===============================================");
console.log("Soal No.2 Membuat Fungsi Kalikan");
console.log("===============================================");

const kalikan = (a, b) => a * b;
 
var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48



// Soal No.3 Membuat Fungsi Introduce
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: “Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!”
console.log("\n===============================================");
console.log("Soal No.3 Membuat Fungsi Introduce");
console.log("===============================================");

const introduce = (name, age, address, hobby) => `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
 
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya hobby yaitu Gaming!" 