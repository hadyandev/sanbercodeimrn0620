var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000;
 
const bacaBuku = i => {
    if(i < books.length){
        readBooksPromise(time, books[i])
        .then(function(fulfilled){
            time = fulfilled
            return bacaBuku(i+1);
        }).catch(function(error){
            console.log(error)
        })
    }
}

bacaBuku(0);