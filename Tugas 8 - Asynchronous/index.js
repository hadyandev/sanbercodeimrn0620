// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

let time = 10000;

const bacaBuku = i => {
    if(i < books.length){
        readBooks(time, books[i], function(callback){
            time = callback
            return bacaBuku(i + 1)
        })
    }
}

bacaBuku(0);