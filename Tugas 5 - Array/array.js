// Soal No.1 Range
console.log("===============================================");
console.log("Soal No.1 Range");
console.log("===============================================");

const range = (startNum, finishNum) => {
    const number = [];
    if(startNum > finishNum){
        for(startNum; startNum >= finishNum; startNum--){
            number.push(startNum);
        }
        return number;
    }else if(finishNum > startNum){
        for(startNum; startNum <= finishNum; startNum++){
            number.push(startNum);
        }
        return number;
    }else{
        return -1;
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal No.2 Range with Step
console.log("\n===============================================");
console.log("Soal No.2 Range with Step");
console.log("===============================================");

const rangeWithStep = (startNum, finishNum, step) => {
    const number = [];
    if(startNum > finishNum){
        for(startNum; startNum >= finishNum; startNum -= step){
            number.push(startNum);
        }
        return number;
    }else if(finishNum > startNum){
        for(startNum; startNum <= finishNum; startNum += step){
            number.push(startNum);
        }
        return number;
    }else{
        return -1;
    }
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal No.3 Sum of Range
console.log("\n===============================================");
console.log("Soal No.3 Sum of Range");
console.log("===============================================");

const sum = (startNum = 0, finishNum = 0, step = 1) => {
    let sum = 0;
    if(startNum > finishNum){
        for(startNum; startNum >= finishNum; startNum -= step){
            sum += startNum;
        }
    }else if(finishNum > startNum){
        for(startNum; startNum <= finishNum; startNum += step){
            sum += startNum;
        }
    }
    return sum;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal No.4 Array Multidimensi
console.log("\n===============================================");
console.log("Soal No.4 Array Multidimensi");
console.log("===============================================");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
];

const dataHandling = array => {
    for(let i = 0; i < array.length; i++){
        console.log(`Nomor ID : ${array[i][0]}`);
        console.log(`Nama Lengkap : ${array[i][1]}`);
        console.log(`TTL : ${array[i][2]} ${array[i][3]}`);
        console.log(`Hobi : ${array[i][4]}\n`);
    }
}

dataHandling(input);


// Soal No.5 Balik Kata
console.log("\n===============================================");
console.log("Soal No.5 Balik Kata");
console.log("===============================================");

const balikKata = word => {
    let newWord = ""
    for(let i = word.length - 1; i >= 0; i--){
        newWord += word[i];
    }
    return newWord;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// Soal No.6 Metode Array
console.log("\n===============================================");
console.log("Soal No.6 Metode Array");
console.log("===============================================");

var input2 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
const dataHandling2 = array => {
    array.splice(1, 1, array[1]+" Elsharawi");
    array.splice(2, 1, "Provinsi "+array[2]);
    array.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(array);
    
    let tanggal = array[3].split("/");
    let bulan = "";
    switch(tanggal[1]){
        case "01": bulan = "Januari"; break;
        case "02": bulan = "Februari"; break;
        case "03": bulan = "Maret"; break;
        case "04": bulan = "April"; break;
        case "05": bulan = "Mei"; break;
        case "06": bulan = "Juni"; break;
        case "07": bulan = "Juli"; break;
        case "08": bulan = "Agustus"; break;
        case "09": bulan = "September"; break;
        case "10": bulan = "Oktober"; break;
        case "11": bulan = "November"; break;
        case "12": bulan = "Desember"; break;
    }
    console.log(bulan);

    tanggal_sort = array[3].split("/").sort((num1, num2) => num2 - num1);
    console.log(tanggal_sort);

    let tanggal_join = tanggal.join("-");
    console.log(tanggal_join);

    let nama = array[1].slice(0, 15);
    console.log(nama);

    
}
dataHandling2(input2);

