// Soal No. 1 (Conditional IF-ELSE)
console.log("===============================================");
console.log("Soal No. 1 (Conditional IF-ELSE)");
console.log("===============================================");
var nama = "Junaedi";
var peran = "Werewolf";

if(nama == "" && peran == ""){
    console.log("Nama harus diisi!");
}else if(nama != "" && peran == ""){
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
}else{
    if(peran == "Penyihir"){
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        console.log(`Halo Penyihir ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
    }else if(peran == "Guard"){
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        console.log(`Halo Guard ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf!`);
    }else if(peran == "Werewolf"){
        console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
        console.log(`Halo Werewolf ${nama}, kamu akan memakan mangsa setiap malam!`);
    }
}

// Soal No.2 (Conditional SWITCH-CASE)
console.log("\n===============================================");
console.log("Soal No.2 (Conditional SWITCH-CASE)");
console.log("===============================================");

var hari = 17; 
var bulan = 8; 
var tahun = 1945;

switch(bulan){
    case 1: bulan = "Januari"; break;
    case 2: bulan = "Februari"; break;
    case 3: bulan = "Maret"; break;
    case 4: bulan = "April"; break;
    case 5: bulan = "Mei"; break;
    case 6: bulan = "Juni"; break;
    case 7: bulan = "Juli"; break;
    case 8: bulan = "Agustus"; break;
    case 9: bulan = "September"; break;
    case 10: bulan = "Oktober"; break;
    case 11: bulan = "November"; break;
    case 12: bulan = "Desember"; break;
}

console.log(`${hari} ${bulan} ${tahun}`);