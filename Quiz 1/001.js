// A. Balik String
console.log("===============================================");
console.log("A. Balik String");
console.log("===============================================");

const balikString = word => {
    let newWord = ""
    for(let i = word.length - 1; i >= 0; i--){
        newWord += word[i];
    }
    return newWord;
}

console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah


// B. Palindrome
console.log("\n===============================================");
console.log("B. Palindrome");
console.log("===============================================");

const palindrome = word => {
    let newWord = ""
    for(let i = word.length - 1; i >= 0; i--){
        newWord += word[i];
    }
    
    if(newWord === word){
        return true;
    }else{
        return false;
    }
}

console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false


// C. Bandingkan Angka
console.log("\n===============================================");
console.log("C. Bandingkan Angka");
console.log("===============================================");

const bandingkan = (num1, num2) => {
    if(num1 < 0 || num2 < 0 || num1 == num2){
        return -1;
    }else if(num1 > num2 || num2 == null){
        return num1
    }else if(num2 > num1){
        return num2
    }
}

console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18


