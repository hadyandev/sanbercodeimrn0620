// A. Ascending Ten
console.log("===============================================");
console.log("A. Ascending Ten");
console.log("===============================================");

const AscendingTen = number => {
    if(number == null){
        return "-1";
    }else{
        let result = "";
        for(let i = number; i < number + 10; i++){
            result += i + " ";
        }
        return result;
    }
}

console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1



// B. Descending Ten
console.log("\n===============================================");
console.log("B. Descending Ten");
console.log("===============================================");

const DescendingTen = number => {
    if(number == null){
        return "-1";
    }else{
        let result = "";
        for(let i = number; i > number - 10; i--){
            result += i + " ";
        }
        return result;
    }
}

console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1


// C. Conditional Ascending Descending
console.log("\n===============================================");
console.log("C. Conditional Ascending Descending");
console.log("===============================================");

const ConditionalAscDesc = (reference, check) => {
    let result = "";
    if(reference == null || check == null){
        return "-1";
    }else if(reference % 2 != 0){
        for(let i = reference; i < reference + 10; i++){
            result += i + " ";
        }
    }else if(reference % 2 == 0){
        for(let i = reference; i > reference - 10; i--){
            result += i + " ";
        }
    }

    return result;
}

console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1


// D. Papan Ular Tangga
console.log("\n===============================================");
console.log("D. Papan Ular Tangga");
console.log("===============================================");

const ularTangga = () => {
    console.log(DescendingTen(100));
    console.log(AscendingTen(81));
    console.log(DescendingTen(80));
    console.log(AscendingTen(61));
    console.log(DescendingTen(60));
    console.log(AscendingTen(41));
    console.log(DescendingTen(40));
    console.log(AscendingTen(21));
    console.log(DescendingTen(20));
    console.log(AscendingTen(1));
}

console.log(ularTangga()) 
/* 
Output : 
  100 99 98 97 96 95 94 93 92 91
  81 82 83 84 85 86 87 88 89 90
  80 79 78 77 76 75 74 73 72 71
  61 62 63 64 65 66 67 68 69 70
  60 59 58 57 56 55 54 53 52 51
  41 42 43 44 45 46 47 48 49 50
  40 39 38 37 36 35 34 33 32 31
  21 22 23 24 25 26 27 28 29 30
  20 19 18 17 16 15 14 13 12 11
  1 2 3 4 5 6 7 8 9 10
*/
