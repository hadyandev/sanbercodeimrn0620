// 1. Class Score
console.log("===============================================");
console.log("1. Class Score");
console.log("===============================================");

class Score{
    constructor(subject, points, email){
        this.subject = subject;
        this.points = points;
        this.email = email;
    }

    predikat(average){
        if(average > 90){
            return "honour"
        }else if(average > 80){
            return "graduate"
        }else if(average > 70){
            return "participant"
        }
    }

    average(){
        const points = this.points;
        const average = points.reduce(function(a, b) {  
            return a + b / points.length;
        }, 0);
        const predikat = this.predikat(average);

        return `Email: ${this.email}\nRata-rata: ${average}\nPredikat: ${predikat}
        `;
    }
}


// 2. Create Score
console.log("\n===============================================");
console.log("2. Create Score");
console.log("===============================================");

const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
]
  
function viewScores(data, subject) {
    let newData = [];
    let index = 0;
    for(let i = 0; i < data[0].length; i++){
        if(data[0][i] == subject){
            index = i;       
        }
    }
    for(let i = 1; i < data.length; i++){
        const obj = {};
        obj.email = data[i][0];
        obj.subject = subject;
        obj.points = data[i][index];
        newData.push(obj);
    }

    console.log(newData);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")


// 3. Rekap Score
console.log("\n===============================================");
console.log("3. Rekap Score");
console.log("===============================================");

function recapScores(data) {
    for(let i = 1; i < data.length; i++){
        const score = new Score(data[0][i],[data[i][1], data[i][2], data[i][3]], data[i][0]);
        console.log(i+". "+score.average());
    }
}
  
recapScores(data);