// Soal No. 1 Looping While
console.log("===============================================");
console.log("Soal No. 1 Looping While");
console.log("===============================================");

console.log("LOOPING PERTAMA");
let looping1 = 2;
while(looping1 <= 20){
    console.log(`${looping1} - I love coding`);
    looping1 += 2;
}
console.log("LOOPING KEDUA");
let looping2 = 20;
while(looping2 >= 2){
    console.log(`${looping2} - I will become a mobile developer`);
    looping2 -= 2;
}


// Soal No.2 Looping Menggunakan For
console.log("\n===============================================");
console.log("Soal No.2 Looping Menggunakan For");
console.log("===============================================");

for (let looping = 1;looping <= 20; looping++){
    // ganjil
    if(looping % 2 != 0){
        // ganjil kelipatan 3
        if(looping % 3 == 0){
            console.log(`${looping} - I Love Coding`);
        }else{
            console.log(`${looping} - Santai`);
        }
    // genap
    }else{
        console.log(`${looping} - Berkualitas`);
    }
}


// Soal No.3 Membuat Persegi Panjang
console.log("\n===============================================");
console.log("Soal No.3 Membuat Persegi Panjang");
console.log("===============================================");

let panjang = 8;
let tinggi = 4;
let sisi = "";

for(let i = 0; i < panjang; i++){
    for(let j = 0; j < tinggi; j++){
        sisi += "#";
        if(sisi.length == panjang){
            console.log(sisi);
            sisi = "";
        }
    }
}


// Soal No.4 Membuat Tangga
console.log("\n===============================================");
console.log("Soal No.4 Membuat Tangga");
console.log("===============================================");

let panjang_tangga = 1;
let tinggi_tangga = 7;
let tangga = "";

for(let i = 0; i < tinggi_tangga; i++){
    for(let j = 0; j < panjang_tangga; j++){
        tangga += "#";
    }
    console.log(tangga);
}


// Soal No.5 Membuat Papan Catur
console.log("\n===============================================");
console.log("Soal No.5 Membuat Papan Catur");
console.log("===============================================");

let size = 8, result = "";

for (let i = 0; i < size; i++) {
    for (let j = 0; j < size; j++) {
        result += (i + j) % 2 ? "#" : " ";
    }
    result += "\n";
}

console.log(result);