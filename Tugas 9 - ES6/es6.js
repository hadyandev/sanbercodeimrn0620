// Soal No.1 Fungsi Arrow
console.log("===============================================");
console.log("Soal No.1 Fungsi Arrow");
console.log("===============================================");

const golden = () => {
    console.log("this is golden!!")
}
   
golden()


// Soal No.2 Object Literal
console.log("\n===============================================");
console.log("Soal No.2 Object Literal");
console.log("===============================================");

const newFunction = function literal(firstName, lastName){
    return {
        firstName,
        lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
            return 
        }
    }
}
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 


// Soal No.3 Desctructuring
console.log("\n===============================================");
console.log("Soal No.3 Desctructuring");
console.log("===============================================");

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)


// Soal No.4 Array Spreading
console.log("\n===============================================");
console.log("Soal No.4 Array Spreading");
console.log("===============================================");

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east];
//Driver Code
console.log(combined)


// Soal No.5 Template Literals
console.log("\n===============================================");
console.log("Soal No.5 Template Literals");
console.log("===============================================");

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 