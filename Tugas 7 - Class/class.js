// Soal No.1 Animal Class - Release 0
console.log("===============================================");
console.log("Soal No.1 Animal Class - Release 0");
console.log("===============================================");

class Animal {
    constructor(name, legs = 4, cold_blooded = false){
        this.name = name;
        this.legs = legs;
        this.cold_blooded = cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


// Soal No.1 Animal Class - Release 1
console.log("\n===============================================");
console.log("Soal No.1 Animal Class - Release 1");
console.log("===============================================");

class Ape extends Animal {
    constructor(name, legs = 2){
        super(name, legs);
    }

    yell(){
        console.log("Auooo");
    }
}

class Frog extends Animal {
    constructor(name){
        super(name);
    }

    jump(){
        console.log("hop hop");
    }
}
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


// Soal No.2 Function to Class
console.log("\n===============================================");
console.log("Soal No.2 Function to Class"); 
console.log("===============================================");

class Clock{
    constructor({template}){
        this.template = template;
    }
  
    render() {
      this.date = new Date();
  
      this.hours = this.date.getHours();
      if (this.hours < 10) this.hours = '0' + this.hours;
  
      this.mins = this.date.getMinutes();
      if (this.mins < 10) this.mins = '0' + this.mins;
  
      this.secs = this.date.getSeconds();
      if (this.secs < 10) this.secs = '0' + this.secs;
  
      this.output = this.template
        .replace('h', this.hours)
        .replace('m', this.mins)
        .replace('s', this.secs);
  
      console.log(this.output);
    }
  
    stop() {
      clearInterval(this.timer);
    };
  
    start() {
      this.render();
      this.timer = setInterval(() => this.render(), 1000);
    };
  
}

var clock = new Clock({template: 'h:m:s'});
clock.start(); 



